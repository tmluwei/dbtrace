package john.walker.log;


/**
 * @author 30san
 *
 */
public final class LogFactory {

	private ILog logger;

	private static LogFactory factory;

	private LogFactory(){};

	private static LogFactory me() {
		if(factory == null) { // 使用双重锁判定，提高效率
			synchronized (LogFactory.class) {
				if(factory == null) {
					factory = new LogFactory();
				}
			}
		}
		return factory;
	}

	public static ILog getLogger() {
		ILog logger =  me().logger;
		if(logger == null) {
			synchronized (LogFactory.class) {
				if(logger == null) {
					setLogger(new MultiLogger(new ConsoleLogger()));
					logger = me().logger;
				}
			}
		}
		return logger;

	}

	public static void setLogger(ILog logger){
		me().logger = logger;
	}


	public static void setLogger(String loggerClassName) throws Exception {
		ILog logger = (ILog) Class.forName(loggerClassName).newInstance();
		setLogger(logger);
	}
}
