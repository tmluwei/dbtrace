package john.walker.spi;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Properties;

import john.walker.ProxyConnection;

/**
 * @author 30san
 *
 */
public class DerbyProxyDriver extends org.apache.derby.jdbc.EmbeddedDriver {

	static {
		try {
			Enumeration<Driver> drivers = DriverManager.getDrivers();
			while(drivers.hasMoreElements()) {
				Driver driver = drivers.nextElement();
				if(org.apache.derby.jdbc.EmbeddedDriver.class.isAssignableFrom(driver.getClass())) {
					DriverManager.deregisterDriver(driver);
				}
			}
			DriverManager.registerDriver(new john.walker.spi.DerbyProxyDriver());
		} catch (SQLException e) {
			// do not care this exception
		}
	}

	public DerbyProxyDriver() throws SQLException {
		super();
	}

	@Override
	public Connection connect(String url, Properties info) throws SQLException {
		return new ProxyConnection(super.connect(url, info));
	}
}
